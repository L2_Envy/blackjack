package L2.Envy.IHG;

import L2.Envy.IHG.Deck.Card;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        //setup
        Scanner scanner = new Scanner(System.in);
        boolean gameover = false;

        //initialize the palyers
        Game game = new Game();
        Card[] dealer = new Card[0];
        Card[] player1 = new Card[0];

        //Deal
        dealer = game.takeHit(dealer);
        player1 = game.takeHit(player1);
        dealer = game.takeHit(dealer);
        player1 = game.takeHit(player1);

        System.out.println("Welcome to BlackJack!");
        //Game loop time
        while(!gameover){
            System.out.println("Opponents hand: " + game.getOpponentsHandAsString(dealer));
            System.out.println("Your Hand: " + game.getHandAsString(player1));
            System.out.println("Options:");
            System.out.println("1: Take a hit");
            System.out.println("2: Hold");
            int option = scanner.nextInt();
            if(option == 2){
                gameover = true;
            }else{
                player1 = game.takeHit(player1);
            }
        }
        if(game.getValueOfHand(player1) > 21 || game.getValueOfHand(dealer) == 21){
            System.out.println("You lost!");
        }else if(game.getValueOfHand(dealer) > 21 && game.getValueOfHand(player1) <=21){
            System.out.println("you won!");
        }//Other possibilites for someone to win or lose
    }
}
