package L2.Envy.IHG;

import L2.Envy.IHG.Deck.Card;
import L2.Envy.IHG.Deck.Suit;
import L2.Envy.IHG.Deck.Value;

import java.util.Random;

/**
 * Created by berry on 8/1/2018.
 */
public class Game {
    private Card[] deck = createDeck();
    private int index = 0;
    public Game(){
        shuffle();
    }
    public Card[] takeHit(Card[] player){
        Card[] hand = new Card[player.length +1];
        //move the old hand array to the new one
        for(int i = 0; i < player.length; i++){
            hand[i] = player[i];
        }
        //add a card
        hand[hand.length-1] = pullCard();
        return hand;
    }

    //This is the hard part, aces can be 1 or 11
    public int getValueOfHand(Card[] hand){
        //keep track of how many aces there are
        int aces =0;
        //The total value variable
        int value = 0;
        //Iterate through hand
        for(Card card : hand){
            Value v = card.getValue();
            //If it is an ace, increment the ace variable
            if(v.equals(Value.ACE)){
                aces++;
            }else{
                //else, grab the raw value
                value += getRawValue(v);
            }
        }
        //Add the aces into the value, treat the aces as a 1 to start
        value += aces;
        //iterate through the amount of aces we have
        for(int i = 0; i < aces; i++){
            //if we are already over with the aces as 1, return the value
            if(value >= 21){
                return value;
                //if the value + 10 is less or equal to 21, we can turn an ace into an 11.
                //The reason we add 10, is because we already added the 1 when we did value += aces
            }else if(value + 10 <= 21){
                value += 10;
            }else{
                //otherwise if we go over, we should just end here, because we know the other aces can't be 11's
                return value;
            }
        }
        //return final value, if we haven't broken yet.
        return value;
    }
    private int getRawValue(Value value){
        switch (value){
            case TWO:
                return 2;
            case THREE:
                return 3;
            case FOUR:
                return 4;
            case FIVE:
                return 5;
            case SIX:
                return 6;
            case SEVEN:
                return 7;
            case EIGHT:
                return 8;
            case NINE:
                return 9;
            case TEN: case JACK: case QUEEN: case KING:
                return 10;
        }
        return 0;
    }
    private void shuffle(){
        Random r = new Random();
        //Just switch two random cards over and over
        for(int i = 0; i < 500; i++){
            int card1 = r.nextInt(52);
            int card2 = r.nextInt(52);
            Card temp = deck[card2];
            deck[card2] = deck[card1];
            deck[card1] = temp;
        }
    }
    //pull the top card, then move the index (acting like it gets removed, but we don't actually have to do anything ;))
    private Card pullCard(){
        Card card = deck[index];
        index++;
        return card;
    }
    private Card[] createDeck(){
        Card[] temp = new Card[52];
        int counter = 0;
        for(Suit suit : Suit.values()){
            for(Value value : Value.values()){
                temp[counter] = new Card(suit, value);
                counter++;
            }
        }
        return temp;
    }
    public String getOpponentsHandAsString(Card[] hand){
        String s = "";
        for(int i = 0; i < hand.length; i++){
            //INSERT LOGIC on how you want to present the cards
        }
        return s;
    }
    public String getHandAsString(Card[] hand){
        String s = "";
        for(int i = 0; i < hand.length; i++){
            //INSERT LOGIC on how you want to present the cards
        }
        return s;
    }
}
