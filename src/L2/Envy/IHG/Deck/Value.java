package L2.Envy.IHG.Deck;

/**
 * Created by berry on 8/2/2018.
 */
public enum Value {
    ACE,
    TWO,
    THREE,
    FOUR,
    FIVE,
    SIX,
    SEVEN,
    EIGHT,
    NINE,
    TEN,
    JACK,
    QUEEN,
    KING;
}
