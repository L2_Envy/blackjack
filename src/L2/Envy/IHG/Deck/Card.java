package L2.Envy.IHG.Deck;

/**
 * Created by berry on 8/1/2018.
 */
public class Card {
    private Value value;
    private Suit suit;
    public Card(Suit suit, Value value){
        this.suit = suit;
        this.value = value;
    }

    public Value getValue() {
        return value;
    }

    public Suit getSuit() {
        return suit;
    }
}
