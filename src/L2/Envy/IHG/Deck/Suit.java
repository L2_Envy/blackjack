package L2.Envy.IHG.Deck;

/**
 * Created by berry on 8/2/2018.
 */
public enum Suit {
    SPADE,
    HEART,
    CLUB,
    DIAMOND
}
